# The Open Hardware Repository `ohwr.org` and GitLab

Since February 2019, the Open Hardware Repository is hosted by our
instance of GitLab Community Edition (GitLab CE) at CERN.

GitLab is a fully integrated software development platform. It supports
the entire project lifecycle with git repositories, issue tracking,
wikis, CI, code review and merge requests.

More documentation and user guides can be found on the official [GitLab
documentation page](https://docs.gitlab.com/ee/user/index.html)

## License

For those who care (we do), GitLab CE is [licenced under the terms of
MIT license](https://docs.gitlab.com/ee/development/licensing.html).
